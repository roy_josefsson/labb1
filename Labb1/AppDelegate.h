//
//  AppDelegate.h
//  Labb1
//
//  Created by Roy Josefsson on 28/01/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

